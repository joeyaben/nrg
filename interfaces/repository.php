<?php

interface repository {
    public function load($id);
    public function save($data);
    public function getAll();
    public function get($field, $value);
}

