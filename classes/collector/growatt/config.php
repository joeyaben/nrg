<?php
namespace collector\growatt;

class config {

    private $version;
    private $index;
    private $value;


    public function __construct($version, $index, $value) {
        $this->version = $version;
        $this->index = $index;
        $this->value = $value;

    }

    public function display() {
        return $this->lookupDesc($this->index) . ": " . $this->value;
    }

    public function getName() {
        return $this->lookupName($this->index);
    }

    public function getValue() {
        return $this->value;
    }

    public function getIndex() {
        return $this->index;
    }

    private function lookupName() {
        $index = $this->index;
        if ($this->version == 1) {
            $config = [
                0x04 => "interval",
                0x05 => "range1",
                0x06 => "range2",
                0x06 => "dataloggerid",
                0x0e => "localip",
                0x0f => "Localport",
                0x10 => "mac",
                0x11 => "serverip",
                0x12 => "serverport",
                0x13 => "serverport",
                0x14 => "version",
                0x1f => "wrong version moron"
            ];
        } else if ($this->version == 5) {
            $config = [
                0x04 => "interval",
                0x05 => "range1",
                0x06 => "range2",
                0x06 => "dataloggerid",
                0x08 => "serial",
                0x09 => "hwversion",
                0x0c => "dns",
                0x11 => "serverip",
                0x12 => "serverport",
                0x13 => "server",
                0x14 => "type",
                0x15 => "swversion",
                0x1f => "basedate",
            ];
        }

        if (isset($config[$index])) {
            return $config[$index];
        } else {
            return "0x" . dechex($index) . "_unknown";
        }
    }

    private function lookupDesc($index) {
        $index = $this->index;
        if ($this->version == 1) {
            $config = [
                0x04 => "Update Interval",
                0x05 => "Modbus Range low",
                0x06 => "Modbus Range high",
                0x06 => "Modbus Address",
                0x0e => "Local IP",
                0x0f => "Local Port",
                0x10 => "Mac Address",
                0x11 => "Server",
                0x12 => "Server Port",
                0x13 => "Server Address",
                0x14 => "Software Version",
            ];
        } else if ($this->version == 5) {
            $config = [
                0x04 => "Update Interval",
                0x05 => "Modbus Range low",
                0x06 => "Modbus Range high",
                0x06 => "Modbus Address",
                0x08 => "Serial Number",
                0x09 => "Hardware Version",
                0x0c => "DNS",
                0x12 => "Server Port",
                0x13 => "Server",
                0x14 => "Device Type",
                0x15 => "Software Version",
                0x1f => "Base Date",
            ];
        }

        if (isset($config[$index])) {
            return $config[$index];
        } else {
            return "Unknown (0x" . dechex($index) . ").";
        }
    }

}


?>
