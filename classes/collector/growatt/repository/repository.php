<?php
namespace collector\growatt\repository;

use db\clause;
use db\db;
use db\insert;
use db\param;
use db\select;

use collector\growatt\measurement;

use DateTime;
use DateTimeZone;
use repository as repositoryInterface;

use PDO;
use stdClass;



abstract class repository implements repositoryInterface {

    public function __construct() {
    }

    public function load($id) {

    }

    public function save($measurement) {
        $qry=new insert(static::TABLE);

        foreach ($measurement->getData() as $label => $value) {
            $param=new param(":" . $label, $value, PDO::PARAM_STR);
            $qry->addParam($param);
        }
        $qry->execute();
    }

    public function getAll() {

    }

    public function get($field, $value) {


    }

    public function getLast() {
        $qry=new select(static::TABLE);
        $qry->addLimit(1);
        $qry->addOrder("datetime DESC");
        $result=db::query($qry);
        return $result->fetchObject("collector\growatt\measurement", array(new static));
    }

    public function getLastDay() {
        $qry=new select(static::TABLE);
        $qry->addFields(array("datetime", "Pac"));
        $qry->addOrder("datetime");
        $qry->where(new clause("datetime >= DATE_SUB(NOW(), INTERVAL 2 DAY)"));
        $result=db::query($qry);
        $measurements=$result->fetchAll(PDO::FETCH_CLASS,"collector\growatt\measurement", array(new static));

        $Pac=new StdClass();
        $Pac->name="Opbrengst";
        $Pac->data=array();

        foreach ($measurements as $measurement) {
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $measurement->datetime, new DateTimeZone("UTC"))->getTimestamp()*1000;
            $Pac->data[]=array($date, (float) $measurement->Pac);
         }
         return array(
            $Pac
         );
    }

    public function getMonthEacToday() {


        $dates = new select(static::TABLE);
        $dates->addFunction(array("date" => "max(datetime)"));
        $dates->addGroupBy("date(datetime)");
        $qry = new select(static::TABLE);
        $qry->addFunction(array("date" => "date(datetime)"));
        $qry->addFields(array("EacToday"));
        $qry->where(clause::inSubQry("datetime", $dates));
        $qry->addLimit(30);
        $qry->addOrder("datetime DESC");

        $reverse = new select(array("month" => $qry));
        $reverse->addOrder("date");
        $result=db::query($reverse);
        $measurements=$result->fetchAll(PDO::FETCH_CLASS,"StdClass");

        $month = new StdClass();
        $month->name="Opbrengst per dag";
        $month->data=array();
        $month->days = array();

        foreach ($measurements as $measurement) {
            $month->days[] = $measurement->date;
            $month->data[]=(float) $measurement->EacToday;
        }
        return $month;
    }

}

