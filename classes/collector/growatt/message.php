<?php
namespace collector\growatt;

require_once("crc16modbus.php");

class message {

    private $msg = "";
    private $remaining = "";

    public $msgid = 1;

    private $type;

    private $inverter;
    private $proto;

    private $isAck = false;

    const ANNOUNCE  = 0x0103;
    const DATA      = 0x0104;
    const PING      = 0x0116;
    const CONFIG    = 0x0118;
    const QUERY     = 0x0119;
    const REBOOT    = 0x0120;
    const ANNOUNCE51= 0x5103;
    const DATA51    = 0x5104;
    const CONFIG51    = 0x5118;
    const QUERY51   = 0x5119;
    const DATE      = 0x5129;

    public function __construct() {
    }

    public static function createFromBuffer($buffer) {
        $msg = new self;
        if (strlen($buffer) > 6) {
            $preamble = substr($buffer, 0, 6);
            $uPreamble=unpack("nmsgid/nver/nsize", $preamble);
            $size = $uPreamble["size"];
            $msg->proto = $uPreamble["ver"];
            $msg->msgid = $uPreamble["msgid"];

            if ($msg->proto == 5) {
                $size += 2;
            }

            if ($msg->proto &&  strlen($buffer) - 5 >= $size) {
                $msg->setMsg(substr($buffer, 0, $size + 6));
                $msg->setRemaining(substr($buffer, $size + 6));
                return $msg;
            } else {
                syslog(LOG_ERR, "Invalid message, expected " . $size . ", got " + strlen($buffer) - 5);
                $msg->remaining="";
            }
        }

    }

    public function create($version, $type, $msg, $init = 1) {
        if ($version == 5) {
            $msg = $this->obfuscate($msg, "Growatt");
            $this->msg = pack("C4n2", 0, $init, 0, $version, strlen($msg) + 2, $type) . $msg;
            $crc = \crc($this->msg);
            $this->msg .= pack("n", $crc);
        } else {
            $this->msg = pack("C4n2", 0, $init, 0, $version, strlen($msg) + 2, $type) . $msg;
        }
    }

    public function obfuscate($msg, $secret) {
        $key = str_pad("", strlen($msg), $secret);
        return $msg ^ $key;
    }


    public function setMsg($msg) {
        $this->msg=$msg;
    }

    public function getMsg() {
        return $this->msg;
    }

    public function getVersion() {
        return $this->proto;
    }

    public function getSize() {
        return strlen($this->msg);
    }

    public function setRemaining($rem) {
        $this->remaining=$rem;
    }

    public function isAck() {
        return (bool) $this->isAck;
    }

    public function getType() {
        return $this->type;
    }

    public function decode() {
        $pre = substr($this->msg, 0, 8);
        $type=unpack("n", substr($this->msg, 6, 2));
        $this->type=$type[1];

        if ($this->proto == 5) {
            $this->msg = $pre . $this->obfuscate(substr($this->msg, 8), "Growatt");
        }

        switch ($this->type) {
            case self::ANNOUNCE:
            case self::ANNOUNCE51:
                if (array_values(unpack("C", substr($this->msg,8,1))) == [ 0 ] ) {
                    $this->isAck = true;
                } else {
                    return($this->decodeAnnounce());
                }
                break;
            case self::DATA:
            case self::DATA51:
                return $this->decodeData();
                break;
            case self::PING:
                $this->type = self::PING;
                return $this->decodePing();
                break;
            case self::CONFIG:
                break;
            case self::QUERY:
            case self::QUERY51:
                break;
            case self::REBOOT:
                break;
            case self::DATE:
                return $this->decodeDate();
                break;
            default:
                syslog(LOG_WARNING, "Received unknown message type 0x" . dechex($this->type));
                break;

        }
    }

    public function getRemaining() {
        $remaining = $this->remaining;
        $this->remaining="";
        return $remaining;
    }

    private function decodeDate() {
        /*
         * There are two possible date announcements, this one comes in after setting the
         * date and time:
         */
        $data = unpack("C/Cinit/C6pre/a10serial/a10ident/a10ident2/C6jib/a19datetime/C*jib5", $this->msg);

        /*
         * ... this one comes in if you do not set date and time. In my current setup, I don't
         * see this variant, but if it will ever show up, the program will choke on it
         * if it turns out to be needed, I will figure out how to distinguish between the two
         * and implement code to recognise either.
         *  $data = unpack("C/Cinit/C6pre/a10serial/a10ident/a10ident2/C23jib0/a10ident3/C19jib1/a9type/C2jib2/a8version/C4jib3/a4hwver/C14jib4/a5tz/a23datetime/C*jib5", $this->msg);
         */
        return array(
            "init"      => $data["init"],
            "serial"    => $data["serial"],
            "ident"     => $data["ident"],
            "ident2"    => $data["ident2"],
            "datetime"  => $data["datetime"],
            "tz"        => ""
        );
        /*
        return array(
            "init"      => $data["init"],
            "serial"    => $data["serial"],
            "ident"     => $data["ident"],
            "ident2"    => $data["ident2"],
            "type"      => $data["type"],
            "version"   => $data["version"],
            "hwver"     => $data["hwver"],
            "tz"        => $data["tz"],
            "datetime"  => $data["datetime"],
        );*/
    }

    private function decodeAnnounce() {
        if ($this->proto == 5) {
            $data = unpack("C/Cinit/C6pre/a10serial/a10ident/C58jib1/a10ident2/C65jib2/a16make/a9type/C*jib3", $this->msg);
        } else {
            $data = unpack("C/Cinit/C6pre/a10serial/a10ident/C58jib1/a10ident2/C62jib2/a20make/a8type/C*jib3", $this->msg);
        }

        return array(
            "init"  => $data["init"],
            "make"  => $data["make"],
            "type"  => $data["type"],
            "serial" => $data["serial"],
            "ident" => $data["ident"],
            "ident2" => $data["ident2"]
        );
    }

    private function decodeData() {
        $software = $this->inverter->getConfig("software");
        return new telegram($this->msg, $this->proto, $software);
    }


    private function decodePing() {
        $data = unpack("C/Cinit/C6pre/a10serial", $this->msg);
        return array(
            "init"   => $data["init"],
            "serial" => $data["serial"],
        );
    }

    public function decodeQueryRequest() {
        $data = unpack("C8pre/a10serial/nfirst/nlast", $this->msg);
        return array(
            "serial" => $data["serial"],
            "first" => $data["first"],
            "last" => $data["last"]
        );
    }

    public function decodeQueryReply() {
        $data = unpack("C8pre/a10serial/nconfigid/nsize", substr($this->msg, 0, 22));

        $value = substr($this->msg, 22, $data["size"]);

        return new config($this->proto, $data["configid"], $value);
    }

    public function setInverter($inv) {
        $this->inverter=$inv;
    }
}



?>
