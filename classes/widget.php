<?php
abstract class widget implements widgetInterface {
    protected $id;

    protected $files=array(
        "css" => "widget.css",
        "js" => array(
            "widget.js",
        ),
        "html" => "widget.html"
    );

    protected $parameters=array();

    protected $paramValues=array();

    protected $repo;

    public function __construct($id, repository $repo) {
        $this->id=(int) $id;
        $this->repo = $repo;
        $this->setParams($this->repo->getWidgetParameters($this->getId()));
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return static::NAME . $this->id;
    }

    public function __toString() {
        $html=file_get_contents("widgets/" . static::NAME . "/" . $this->files["html"]);
        $params=$this->getParameterValues();
        foreach ($params as $param => $value) {
            $html=str_replace("{" . $param . "}", $value, $html);
        }
        return $html;
    }

    public function getJS() {
        $return=array();
        foreach ($this->files["js"] as $js) {
            if (substr($js, 0, 4) == "http") {
                $return[]=$js;
            } else {
                $return[]="widgets/" . static::NAME . "/" . $js;
            }
        }
        return $return;
    }

    public function getCSS() {
        return "widgets/" . static::NAME . "/" . $this->files["css"];
    }

    public function getParameters() {
        return $this->parameters;
    }

    public function getParameterValues() {
        $params=$this->paramValues;
        $params["widgetid"]=$this->id;
        return $params;
    }

    public function getParam($param) {
        return $this->paramValues[$param];
    }

    public function setParams(array $params) {
        $this->paramValues=$params;
    }

    abstract public function getUpdate();

}
