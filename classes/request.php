<?php
class request {

    private $getvars;
    private $postvars;
    private $requestvars;
    private $sessionvars;

    public function __construct($get=null, $post=null, $request=null, $session=null) {
        if (is_null($get)) {
            $get=$_GET;
        }

        if (is_null($post)) {
            $post=$_POST;
        }

        if (is_null($get)) {
            $request=$_REQUEST;
        }

        if (is_null($get)) {
            $session=$_SESSION;
        }

        $this->getvars=$get;
        $this->postvars=$post;
        $this->requestvars=$request;
        $this->sessonvars=$session;
    }

    public function get($name) {
        if (isset($this->getvars[$name])) {
            return $this->getvars[$name];
        } else {
            throw new Exception("var not found");
        }
    }


}

?>
