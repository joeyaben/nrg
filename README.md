NRG
===
Monitor energy usage and generation
-----------------------------------
_(no :cloud: needed)_

# Supported Devices #
- DSMR (Dutch SmartMeter Requirements)
- Growatt
    - Tested with Growatt 3000s and "ShineLAN Box"
    - briefly tested with "Ethernet kit" - which worked but only 5 minute intervals, this should be easy to fix though.
    - It supports v1.5 of the Growatt protocol.
        - It should also support v1.2, but that has not been tested, so there are problably a few issues here and there. Let me know if you face this problem.

Let me know if you want to help adding other devices.

# Project Status #
**Absolute Alpha state!**
The software seems quite stable. The DSMR part has been running on my system without much problems for more than 4 years. The Growatt part I have added about a month ago and it has run without problems as well. However, there is no installation script, there may be things hardcoded that should not and not all functionality one would expect from a application like this is available.

If you're uncomfortable using the commandline, typing queries in MySQL and needing to fiddle with things to get it working, you should probably not try to use NRG. Feel free to drop me a line though, maybe it will inspire me to spend some time improving things.

If you are comfortable with a little hacking here and there but you still can't get it to work? Please don't hesitate to contact me, I'll be happy to help.
# Requirements #
- Apache
- PHP (7.3)
- MySQL or MariaDB (all recent versions should work)
- Highcharts (http://highcharts.com) and JQuery
    Automatically downloaded when using the web interface. If you want a fully Cloud-Free&#174; installation, it should be easy to change this
- For DSMR: serial to USB connection, connected on `/dev/ttyUSB0`. This is currently hardcoded, let me know if you need it changed.
- For Growatt: Use your DNS server to redirect `server.growatt.com` to the system running NRG.

# Features #
- Near real time monitoring of Dutch Smart meter
- Per minute monitoring of Growatt Inverter
- If your gasmeter is connected to your smart meter, it will report its readings once per hour
- Storing up to two hours of measurements in in-memory database to prevent disk wear
- Writing data to on-disk database each hour
- Aggregation of data to per-minute and per-hour intervals to reduce database size for DSMR.
- Configurable dashboards

# TODO #
- [ ] Proper installation script
- [ ] Configuring dashboards through user friendly interface instead of SQL queries
- [ ] Aggregation of Growatt data
- [ ] See if there can be more shared code between DSMR and Growatt
- [ ] Provide instructions and API for easier adding of other devices
- [ ] Dashboards for daily, weekly, monthly and other historic views
- [ ] Dashboards for gasmeter readings
- [ ] Configuration screen
- [ ] Logon screen
    ** until that moment it is highly discouraged to run this on the Internet without some other protection **
- [ ] Better re-use of dashboard widgets
- [ ] Create systemd startup script for collectors
- [ ] Flush data from in-memory DB to disk in case of shutdown
- [ ] Change the not-so-nifty chosen "current" for "now" in some of the database fields
- [ ] Some form of alarming when readings are no longer produced
- [ ] Influxdb support?
- [ ] Support for multiple smartmeters and / or inverters
    - The collection part should support this currently: all data is stored with a serial number, however, the dashboards currently display *all* data, which is likely to be become a mess if you have more than one smart meter or more than one inverter.
- [ ] There is some hardcoded Dutch language here and there
- [ ] ...

If you want to work on any of the above, please contact me. If you have additional ideas, please create an issue on GitLab.

# License #
Unless otherwise stated, all files are under the AGPLv3.0. This basicly means that you can
- Use
- Copy
- Modify
and even sell it if you want as long as: you don't change the license and share your changes.

If it breaks, you get to keep all the pieces. If it blows up your smartmeter, inverter, server or cat, I'm not responsible. 

If you want to write code for this project, it should be AGPLv3.0 licensed. If you include third party software, it should at least be GPLv2 compatible. I am aware that Highcharts currently does not fit this requirement.

# Installation #
- Download the source code from gitlab
- Unpack in a directory *outside* your webroot.
- Rename `include\dbconnect.php.dist` to `include\dbconnect.php` and change for your needs:
```
db::setLoginDetails("localhost", "nrg", "nrg", "secret", "");
```
(servername, databasename, username, password, prefix)
_prefix_ can be used if you cannot create a seperate database for nrg and wish to use a shared database, all table names will be prefixed with `prefix_`. This is currently untested.
- Create a database, user and assign privileges to the user
- Create all the necessary tables by importing the SQL scripts in the `sql/` directory
- Create dashboards. This is currently undocumented, but I have provided `sql/dashboard_current.sql` for my current dashboards.
- Create a virtual server or directory pointing to the `web/` directory inside the `nrg` directory.
    ** It is recommended that you do not serve this to the "outside world" without additional protection. There currently is no login system or password protection. **
- Start the collectors for DSMR and/or Growatt. There currently is no provided `systemd` script, I run them in a `screen` session. I have a working `systemd` script for the DSMR collection, that I will adept to start either or both. Let me know if you need it.


# Thanks #
- I could not have gotten this to work without the work by @scurious on https://github.com/sciurius/Growatt-WiFi-Tools and https://www.vromans.org/software/sw_growatt_wifi_protocol.html




