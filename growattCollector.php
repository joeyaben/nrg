#!/usr/bin/php
<?php
require_once("include/include.php");

use collector\growatt\collector;

pcntl_async_signals(true);
pcntl_signal(SIGINT, array("collector\growatt\collector", "signalHandler"));
pcntl_signal(SIGTERM, array("collector\growatt\collector", "signalHandler"));
pcntl_signal(SIGHUP, array("collector\growatt\collector", "signalHandler"));

set_time_limit(0);

$address = '192.168.1.221';
$port = 5279;

$gwc = new collector($address, $port);
$gwc->handleIncoming();

?>
