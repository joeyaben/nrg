<?php
class meterR extends widget implements widgetInterface {
    const NAME="meterR";

    protected $files=array(
        "css" => "widget.css",
        "js" => array(
                "/widget.js",
                "http://code.jquery.com/jquery-1.9.1.js"
            ),
        "html" => "widget.html"
    );

    public function getUpdate() {
        return array("current" => "meterR.update(data)");
    }

}
