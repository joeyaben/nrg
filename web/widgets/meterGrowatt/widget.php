<?php
class meterGrowatt extends widget implements widgetInterface {
    const NAME="meterGrowatt";

    protected $files=array(
        "css" => "widget.css",
        "js" => array(
                "/widget.js",
                "http://code.jquery.com/jquery-1.9.1.js"
            ),
        "html" => "widget.html"
    );

    public function getUpdate() {
        return array("currentGrowatt" => "meterGrowatt.update(data)");
    }

}
