<?php
class growattBarGraph extends widget implements widgetInterface {
    const NAME="growattBarGraph";

    protected $files=array(
        "css" => "widget.css",
        "js" => array(
                "widget.js",
                "http://code.jquery.com/jquery-1.9.1.js",
                "http://code.highcharts.com/highcharts.js",
                "http://code.highcharts.com/highcharts-more.js"
            ),
        "html" => "widget.html"
    );

    protected $parameters=array(
    );

    public function getUpdate() {
        return array();
    }

}
