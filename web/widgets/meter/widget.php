<?php
class meter extends widget implements widgetInterface {
    const NAME="meter";

    protected $files=array(
        "css" => "widget.css",
        "js" => array(
                "/widget.js",
                "http://code.jquery.com/jquery-1.9.1.js"
            ),
        "html" => "widget.html"
    );

    public function getUpdate() {
        return array("current" => "meter.update(data)");
    }

}
