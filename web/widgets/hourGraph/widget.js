var hourGraph=function() {
    function graph(container, title) {
        var data=[]
        $.ajax({
            url: "getJSON.php?data=lasthour",
            async: false,
            dataType: "json",
            success: function(json) {
                data=json;
            }
        });
        return new Highcharts.Chart({
            chart: {
                type: 'spline',
                renderTo: container,
                useUTC: false 
            },
            title: {
                text: title
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                min: 0
            },
            plotOptions: {
                spline: {
                    marker: {
                        enabled: false
                    }
                }
            },
            series: data
        });
    }

    function update(graph, fields, data) {
        var graph1 = graph.series[0];
        var graph2 = graph.series[1];
        var graph3 = graph.series[2];
        var graph4 = graph.series[3];
        var time=new Date(data.datetime.replace(" ", "T")).getTime() - (new Date().getTimezoneOffset() * 60000);

        graph1.addPoint([time,parseFloat(data[fields[0]])*1000]);
        graph2.addPoint([time,parseFloat(data[fields[1]])*1000]);
        graph3.addPoint([time,parseFloat(data[fields[2]])*1000]);
        graph4.addPoint([time, parseFloat(data[fields[3]])*1000]);

        [graph1, graph2, graph3, graph4].forEach(function(graph) {
            var first=graph.points[0];
            if(first.x < (time - (60 * 60 * 1000))) {
                first.remove();
            }
        });
    }

    return {
        graph:graph,
        update:update
    }
}();
