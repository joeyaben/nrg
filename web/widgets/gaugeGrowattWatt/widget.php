<?php
class gaugeGrowattWatt extends widget implements widgetInterface {
    const NAME="gaugeGrowattWatt";

    protected $files=array(
        "css" => "widget.css",
        "js" => array(
                "widget.js",
                "http://code.jquery.com/jquery-1.9.1.js",
                "http://code.highcharts.com/highcharts.js",
                "http://code.highcharts.com/highcharts-more.js"
            ),
        "html" => "widget.html"
    );

    protected $parameters=array(
        "field",
        "max",
        "title"
    );

    public function getUpdate() {
        $field=$this->getParam("field");
        return array("currentGrowatt" => "gaugeGrowattWatt.update(" . $this->getName() . ",'" . $field . "', data)");
    }

}
