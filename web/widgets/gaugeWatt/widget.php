<?php
class gaugeWatt extends widget implements widgetInterface {
    const NAME="gaugeWatt";

    protected $files=array(
        "css" => "widget.css",
        "js" => array(
                "widget.js",
                "http://code.jquery.com/jquery-1.9.1.js",
                "http://code.highcharts.com/highcharts.js",
                "http://code.highcharts.com/highcharts-more.js"
            ),
        "html" => "widget.html"
    );

    protected $parameters=array(
        "field",
        "max",
        "title"
    );

    public function getUpdate() {
        $field=$this->getParam("field");
        $fieldR=$this->getParam("fieldR");
        return array("current" => "gaugeWatt.update(" . $this->getName() . ",'" . $field . "', '" . $fieldR . "', data)");
    }

}
