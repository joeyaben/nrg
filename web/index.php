<?php
ini_set("display_errors", true);
error_reporting(E_ALL);
set_include_path("..");
require_once("include/include.php");
require_once("include/init.php");

$request=new request();

$dashboard=new dashboard(new dashboardRepository);
try {
    $dashboard=$dashboard->getByName($request->get("name"));
} catch (Exception $e) {
    $dashboard=$dashboard->getByName("today");
}

if ($dashboard instanceof dashboard) {
    echo $dashboard->display();
}
