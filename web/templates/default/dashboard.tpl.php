<?php
if (!NRG) {
    die("Template");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->getHead(); ?>
    </head>
    <body>
        <nav class="dashboard">
            <ul>
                <?php foreach ($tpl_nav as $nav): ?>
                    <li><a href="?name=<?= $nav->getName() ?>"><?= $nav->getTitle() ?></a></li>
                <?php endforeach ?>
            </ul>
        </nav>
        <?php foreach ($tpl_blocks as $row): ?>
            <div class="row">
                <?php foreach ($row as $block): ?>
                    <?= $block ?>
                <?php endforeach ?>
            </div>
        <?php endforeach ?>
    </body>
</html>
